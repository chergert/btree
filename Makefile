all: btree

PKGS = glib-2.0

OBJECTS =
OBJECTS += btree.o
OBJECTS += test.o

WARNINGS =
WARNINGS += -Wall
WARNINGS += -Werror

FLAGS =
FLAGS += -g

%.o: %.c
	$(CC) -o $@ -c $(WARNINGS) $(FLAGS) $(shell pkg-config --cflags $(PKGS)) $*.c

btree: $(OBJECTS)
	$(CC) -o $@ $(WARNINGS) $(FLAGS) $(OBJECTS) $(shell pkg-config --libs $(PKGS))

clean:
	rm -f *.o btree
