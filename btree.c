#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "btree.h"

#define MAX_KEYS 11
#define MIN_KEYS  5

typedef struct
{
   gint count;
   BtreeItem key[MAX_KEYS];
   glong branch[MAX_KEYS + 1];
} BtreeNode;

struct _Btree
{
   gchar mode;
   guint node_size;
   glong num_items;
   glong num_nodes;
   glong root;
   int fd;
   BtreeNode current_node;
};

Btree *
btree_new (const gchar *filename,
           gchar        mode)
{
   Btree *btree;
   int ret;

   g_return_val_if_fail(filename, NULL);
   g_return_val_if_fail(mode == 'r' || mode == 'w', NULL);

   btree = g_new0(Btree, 1);
   btree->mode = mode;
   btree->node_size = sizeof(BtreeNode);

   if (mode == 'r') {
      btree->fd = open(filename, O_RDONLY);
      if (btree->fd == -1) {
         g_error("File cannot be opened.");
      }

      ret = read(btree->fd, &btree->current_node, sizeof btree->current_node);
      if (ret != sizeof btree->current_node) {
         /*
          * Assume the Btree is empty if we cannot read the file.
          */
         btree->num_items = 0;
         btree->num_nodes = 0;
         btree->root = -1;
      } else {
         /*
          * Node zero is not a normal node, it contains the following.
          */
         btree->num_items = btree->current_node.branch[0];
         btree->num_nodes = btree->current_node.branch[1];
         btree->root = btree->current_node.branch[2];
      }
   } else if (mode == 'w') {
      btree->fd = open(filename, O_RDWR | O_CREAT, 0700);
      if (btree->fd == -1) {
         g_error("File cannot be opened.");
      }

      btree->root = -1;
      btree->num_items = 0;
      btree->num_nodes = 0;
      btree->current_node.branch[0] = btree->num_items;
      btree->current_node.branch[1] = btree->num_nodes;
      btree->current_node.branch[2] = btree->root;

      lseek(btree->fd, 0, SEEK_SET);
      ret = write(btree->fd, (gchar *)&btree->current_node,
                  sizeof btree->current_node);
      if (ret == -1) {
         g_error("File could not be written.");
      }
   } else {
      g_assert_not_reached();
   }

   return btree;
}

void
btree_free (Btree *btree)
{
   gint ret;

   g_return_if_fail(btree != NULL);

   if (btree->mode == 'w') {
      /*
       * Be sure to write out the updated node zero:
       */
      memset(&btree->current_node, 0, sizeof btree->current_node);
      btree->current_node.branch[0] = btree->num_items;
      btree->current_node.branch[1] = btree->num_nodes;
      btree->current_node.branch[2] = btree->root;

      lseek(btree->fd, 0, SEEK_SET);
      ret = write(btree->fd, (gchar *)&btree->current_node,
                  sizeof btree->current_node);
      if (ret != sizeof btree->current_node) {
         g_error("File could not be written.");
      }
   }

   close(btree->fd);
   free(btree);
}

gboolean
btree_empty (Btree *btree)
{
   g_return_val_if_fail(btree != NULL, FALSE);
   return (btree->root == -1);
}

static gboolean
btree_search_node (Btree    *btree,
                   BtreeKey *target,
                   gint     *location)
{
   gboolean found = FALSE;

   g_return_val_if_fail(btree != NULL, FALSE);
   g_return_val_if_fail(target != NULL, FALSE);
   g_return_val_if_fail(location != NULL, FALSE);

   if (g_strcmp0(*target, btree->current_node.key[0].key) < 0) {
      *location = -1;
   } else {
      /*
       * Do a sequential search, right to left.
       */
      *location = btree->current_node.count - 1;
      while ((g_strcmp0(*target, btree->current_node.key[*location].key) < 0) && ((*location) > 0)) {
         (*location)--;
      }
      if (g_strcmp0(*target, btree->current_node.key[*location].key) == 0) {
         found = TRUE;
      }
   }

   return found;
}

static void
btree_add_item (Btree           *btree,
                const BtreeItem *new_item,
                glong            new_right,
                BtreeNode       *node,
                gint             location)
{
   gint j;

   g_return_if_fail(btree);
   g_return_if_fail(new_item);
   g_return_if_fail(node);

   for (j = node->count; j > location; j--) {
      node->key[j] = node->key[j - 1];
      node->branch[j + 1] = node->branch[j];
   }

   node->key[location] = *new_item;
   node->branch[location + 1] = new_right;
   node->count++;
}

static void
btree_split (Btree           *btree,
             const BtreeItem *current_item,
             glong            current_right,
             glong            current_root,
             gint             location,
             BtreeItem       *new_item,
             glong           *new_right)
{
   BtreeNode right_node = { 0 };
   gint j;
   gint median;

   g_return_if_fail(btree);
   g_return_if_fail(current_root);
   g_return_if_fail(new_item);
   g_return_if_fail(new_right);

   if (location < MIN_KEYS) {
      median = MIN_KEYS;
   } else {
      median = MIN_KEYS + 1;
   }

   lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
   read(btree->fd, &btree->current_node, sizeof btree->current_node);

   for (j = median; j < MAX_KEYS; j++) {
      /*
       * Move half of the items to the right_node.
       */
      right_node.key[j - median] = btree->current_node.key[j];
      right_node.branch[j - median + 1] = btree->current_node.branch[j + 1];
   }

   right_node.count = MAX_KEYS - median;
   btree->current_node.count = median;

   /*
    * Put current_item in place.
    */
   if (location < MIN_KEYS) {
      btree_add_item(btree, current_item, current_right,
                     &btree->current_node, location + 1);
   } else {
      btree_add_item(btree, current_item, current_right,
                     &right_node, location - median + 1);
   }

   *new_item = btree->current_node.key[btree->current_node.count - 1];
   right_node.branch[0] = btree->current_node.branch[btree->current_node.count];
   btree->current_node.count--;

   lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
   write(btree->fd, &btree->current_node, sizeof btree->current_node);

   btree->num_nodes++;
   *new_right = btree->num_nodes;
   lseek(btree->fd, (*new_right) * sizeof(BtreeNode), SEEK_SET);
   write(btree->fd, &right_node, sizeof right_node);
}

static void
btree_push_down (Btree           *btree,
                 const BtreeItem *current_item,
                 glong            current_root,
                 gboolean        *move_up,
                 BtreeItem       *new_item,
                 glong           *new_right)
{
   gint location = 0;

   g_return_if_fail(btree);
   g_return_if_fail(current_item);
   g_return_if_fail(move_up);
   g_return_if_fail(new_item);
   g_return_if_fail(new_right);

   if (current_root == -1) { /* Stopping case */
      /*
       * Cannot insert into an empty tree.
       */
      *move_up = TRUE;
      *new_item = *current_item;
      *new_right = -1;
   } else { /* Recursive case */
      lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
      read(btree->fd, &btree->current_node, sizeof btree->current_node);

      if (btree_search_node(btree, (BtreeKey *)&current_item->key, &location)) {
         g_error("Error: Attempt to put a duplicate into Btree.");
      }

      btree_push_down(btree, current_item,
                      btree->current_node.branch[location + 1],
                      move_up, new_item, new_right);

      if (*move_up) {
         lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
         read(btree->fd, &btree->current_node, sizeof btree->current_node);

         if (btree->current_node.count < MAX_KEYS) {
            *move_up = FALSE;
            btree_add_item(btree, new_item, *new_right, &btree->current_node,
                           location + 1);
            lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
            write(btree->fd, &btree->current_node, sizeof btree->current_node);
         } else {
            *move_up = TRUE;
            btree_split(btree, new_item, *new_right, current_root, location,
                        new_item, new_right);
         }
      }
   }
}

gboolean
btree_insert (Btree           *btree,
              const BtreeItem *item)
{
   BtreeItem new_item = {{ 0 }};
   gboolean move_up = FALSE;
   glong new_right = -1;

   g_return_val_if_fail(btree, FALSE);
   g_return_val_if_fail(item, FALSE);

   btree_push_down(btree, item, btree->root, &move_up,
                   &new_item, &new_right);

   if (move_up) {
      btree->current_node.count = 1;
      btree->current_node.key[0] = new_item;
      btree->current_node.branch[0] = btree->root;
      btree->current_node.branch[1] = new_right;
      btree->num_nodes++;
      btree->root = btree->num_nodes;
      lseek(btree->fd, btree->num_nodes * sizeof(BtreeNode), SEEK_SET);
      write(btree->fd, &btree->current_node, sizeof btree->current_node);
   }

   btree->num_items++;
   return TRUE;
}

gboolean
btree_retrieve (Btree     *btree,
                BtreeKey  *search_key,
                BtreeItem *item)
{
   gboolean found;
   glong current_root;
   gint location;

   g_return_val_if_fail(btree, FALSE);
   g_return_val_if_fail(search_key, FALSE);
   g_return_val_if_fail(item, FALSE);

   found = FALSE;
   current_root = btree->root;

   while ((current_root != -1) && (!found)) {
      lseek(btree->fd, current_root * sizeof(BtreeNode), SEEK_SET);
      read(btree->fd, &btree->current_node, sizeof btree->current_node);

      if (btree_search_node(btree, search_key, &location)) {
         found = TRUE;
         *item = btree->current_node.key[location];
      } else {
         current_root = btree->current_node.branch[location + 1];
      }
   }

   return found;
}
