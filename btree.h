#ifndef B_TREE_H
#define B_TREE_H

#include <glib.h>

#define KEY_MAX  12
#define DATA_MAX 36

typedef struct _Btree     Btree;
typedef struct _BtreeItem BtreeItem;
typedef gchar             BtreeKey[KEY_MAX];
typedef gchar             BtreeData[DATA_MAX];

struct _BtreeItem
{
   BtreeKey  key;
   BtreeData data;
};

gboolean  btree_empty    (Btree           *btree);
void      btree_free     (Btree           *btree);
gboolean  btree_insert   (Btree           *btree,
                          const BtreeItem *item);
Btree    *btree_new      (const gchar     *filename,
                          gchar            mode);
gboolean  btree_retrieve (Btree           *btree,
                          BtreeKey        *search_key,
                          BtreeItem       *item);

#endif /* B_TREE_H */
