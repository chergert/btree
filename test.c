#include <glib/gprintf.h>
#include <stdlib.h>
#include <string.h>

#include "btree.h"

gint
main (gint   argc,
      gchar *argv[])
{
   Btree *btree;
   BtreeItem item;
   BtreeKey key;
   gchar mode = 'w';
   guint i;

   if (g_file_test("btree.dat", G_FILE_TEST_IS_REGULAR)) {
      mode = 'r';
   }

   btree = btree_new("btree.dat", mode);
   g_assert(btree);

   if (btree_empty(btree)) {
      g_print("Btree is empty.\n");
   }

   for (i = 0; i < 1000; i++) {
      if (mode == 'w') {
         memset(&item, 0, sizeof item);
         g_snprintf(item.key, sizeof item.key, "%u", i);
         g_snprintf(item.data, sizeof item.data, "%u", 1000 - i);
         if (!btree_insert(btree, &item)) {
            g_error("Failed to insert into Btree.");
         }
      } else {
         g_snprintf(item.key, sizeof item.key, "%u", i);
         if (!btree_retrieve(btree, &item.key, &item)) {
            g_error("Failed to lookup in Btree.");
         }
      }
   }

   for (i = 0; i < 1000; i++) {
      memset(&item, 0, sizeof item);
      g_snprintf(key, sizeof key, "%u", i);
      if (!btree_retrieve(btree, &key, &item)) {
         g_error("Failed to lookup %s", item.key);
      }
      //g_debug("Found value: %s", item.data);
   }

   btree_free(btree);

   return EXIT_SUCCESS;
}
